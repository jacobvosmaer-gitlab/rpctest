module gitlab.com/jacobvosmaer-gitlab/rpctest

go 1.15

require (
	github.com/golang/protobuf v1.5.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	gitlab.com/gitlab-org/gitaly v1.87.0
	google.golang.org/grpc v1.37.0
	google.golang.org/grpc/examples v0.0.0-20210430044426-28078834f35b
)
