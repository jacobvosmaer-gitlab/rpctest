# rpctest

This code is part of an [experiment to test whether gRPC can sometimes
be a bottleneck for Gitaly
throughput](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/1046).
It implements toy versions of Gitaly and GitLab Workhorse to the point
where it can handle Git HTTP fetch requests for a single repository,
optionally with pack-objects caching.

This is not production code. The purpose of this code is to explore
the limits of what we can do with Git fetch traffic.

## Usage

### frontend

`frontend` emulates GitLab Workhorse. It implements just enough to
make it possible to run `git clone http://FRONTEND_ADDRESS/repo.git`.
Note that `/repo.git` is hard-coded. It can only listen on a TCP
socket and it does not do encryption.

The executable takes two command line options: the listen address for
HTTP server and the network address of the `rpc` server (see below).

```
./frontend LISTEN_ADDRESS RPC_ADDRESS
```

### rpc

`rpc` emulates Gitaly. It supports only 3 RPC's: GetInfoRefs,
PostUploadPack and PackObjectsHook. It only provides access to one
repository which it assumes to be the current working directory of the
`rpc` process.

```
cd /path/to/repo.git
HOOK_PATH=/path/to/hook /path/to/rpc LISTEN_ADDRESS
```

`rpc` can only listen on TCP.

#### pack-objects cache

`rpc` contains a buggy implementation of pack-objects caching. To
enable this cache, set `USE_CACHE=1` when launching `rpc`.

```
USE_CACHE=1 HOOK_PATH=/path/to/hook /path/to/rpc LISTEN_ADDRESS
```

This cache writes its files in `/tmp/pack-objects-cache`. It never
expires entries. When multiple identical requests come in at once
something goes wrong which I don't understand; I have been forced to
reboot the server to get rid of this failure mode. SIGKILL did not
work. To prevent this from happening, seed the cache with a single
request before starting up a benchmark.

#### zero-copy mode

It is possible to let `git-upload-pack` write its output straight to
the network socket, without letting `rpc` copy it through user space.
This reduces CPU and increases throughput, but it has the downside
that you cannot use TLS because the Linux kernel cannot do TLS for us.
You need to go through user space for that.

```
ZERO_COPY=1 HOOK_PATH=/path/to/hook /path/to/rpc LISTEN_ADDRESS
```

## Transport

The `transport` directory contains the implementation of a very
minimal client/server protocol. Clients initiate connections with the
`Call` function and send an opaque blob of bytes that is the
"request". The server has a handler function that can accept or reject
the request blob. If the handler accepts, the client and it can
communicate via a Go `net.Conn` object which is essentially a TCP
socket. If the handler rejects the request the client receives an
error.

The minimalism of this protocol is intentional, it was part of the
experiment. I wanted to see if removing layers could make the system
faster, and for that I wanted a transport as thin as possible. I
arrived at this protocol by working my way up from a plain TCP
connection. (A TCP connection on its own is too minimal because the
server would not know what the client wants to do.)
