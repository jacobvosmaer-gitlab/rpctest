package transport

import (
	"errors"
	"fmt"
	"log"
	"net"
	"strings"
	"sync"
	"time"
)

const (
	responseOK  = "ok"
	errorPrefix = "error: "
)

// Handler is used by servers. It is where the application-specific things happen.
type Handler func(session *ServerSession, request []byte) error

// DialFunc is a convenience to make Call easier to work with.
type DialFunc func(time.Duration) (net.Conn, error)

// DialNet returns a DialFunc that calls net.DialTimeout.
func DialNet(network, address string) DialFunc {
	return func(t time.Duration) (net.Conn, error) { return net.DialTimeout(network, address, t) }
}

// Call dials the given network and address, presents the request to the
// server and returns the connection once the server accepts the request.
// The caller must call Close on the returned connection.
func Call(dial DialFunc, request []byte) (_ net.Conn, err error) {
	deadline := time.Now().Add(defaultHandshakeTimeout)

	c, err := dial(defaultHandshakeTimeout)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err != nil {
			c.Close()
		}
	}()

	if err := sendFrame(c, []byte(request), deadline); err != nil {
		return nil, fmt.Errorf("Call: send request: %v", err)
	}

	response, err := recvFrame(c, deadline)
	if err != nil {
		return nil, fmt.Errorf("Call: receive response: %v", err)
	}
	if errString := string(response); errString != responseOK {
		return nil, fmt.Errorf("request rejected: %s", strings.TrimPrefix(errString, errorPrefix))
	}

	return c, nil
}

type Server struct {
	listeners        []net.Listener
	handler          Handler
	m                sync.Mutex
	connWg           sync.WaitGroup
	handshakeTimeout time.Duration
}

const defaultHandshakeTimeout = 10 * time.Second

func NewServer(handler Handler) *Server {
	return &Server{
		handler:          handler,
		handshakeTimeout: defaultHandshakeTimeout,
	}
}

func (s *Server) Stop() {
	s.m.Lock()
	defer s.m.Unlock()
	for _, l := range s.listeners {
		l.Close()
	}
	s.listeners = nil
}

func (s *Server) GracefulStop() {
	s.Stop()
	s.connWg.Wait()
}

func (s *Server) Serve(l net.Listener) error {
	s.m.Lock()
	s.listeners = append(s.listeners, l)
	s.m.Unlock()

	var tempDelay time.Duration // how long to sleep on accept failure
	for {
		c, err := l.Accept()
		if err != nil {
			// Accept retry logic borrowed from Go stdlib net/http server.
			if ne, ok := err.(net.Error); ok && ne.Temporary() {
				if tempDelay == 0 {
					tempDelay = 5 * time.Millisecond
				} else {
					tempDelay *= 2
				}

				if max := 1 * time.Second; tempDelay > max {
					tempDelay = max
				}

				log.Printf("rpc: Accept error: %v; retrying in %v", err, tempDelay)
				time.Sleep(tempDelay)
				continue
			}

			return err
		}

		tempDelay = 0
		s.connWg.Add(1)
		go s.handleConnection(c)
	}
}

func (s *Server) handleConnection(c net.Conn) {
	defer s.connWg.Done()
	defer c.Close()

	deadline := time.Now().Add(s.handshakeTimeout)
	req, err := recvFrame(c, deadline)
	if err != nil {
		return
	}

	session := &ServerSession{
		c:        c,
		deadline: deadline,
	}
	if err := s.handler(session, req); err != nil {
		session.reject(err)
	}
}

// ServerSession wraps an incoming connection whose handshake has not
// been completed yet.
type ServerSession struct {
	c        net.Conn
	accepted bool
	deadline time.Time
}

// Accept completes the handshake on the connection wrapped by ss and
// unwraps the connection.
func (ss *ServerSession) Accept() (net.Conn, error) {
	if ss.accepted {
		return nil, errors.New("connection already accepted")
	}

	ss.accepted = true
	if err := sendFrame(ss.c, []byte(responseOK), ss.deadline); err != nil {
		return nil, err
	}

	return ss.c, nil
}

func (ss *ServerSession) reject(err error) {
	if ss.accepted {
		return
	}

	_ = sendFrame(ss.c, []byte(errorPrefix+err.Error()), ss.deadline)
}
