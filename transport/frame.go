package transport

import (
	"encoding/binary"
	"errors"
	"io"
	"net"
	"time"
)

const (
	maxFrameSize = (1 << 20) - 1
)

var (
	errFrameTooLarge = errors.New("frame too large")
)

func sendFrame(c net.Conn, frame []byte, deadline time.Time) error {
	if len(frame) > maxFrameSize {
		return errFrameTooLarge
	}

	if err := c.SetWriteDeadline(deadline); err != nil {
		return err
	}

	hdr := make([]byte, 4)
	binary.BigEndian.PutUint32(hdr, uint32(len(frame)))
	if _, err := c.Write(hdr); err != nil {
		return err
	}
	if _, err := c.Write(frame); err != nil {
		return err
	}

	if err := removeDeadline(c); err != nil {
		return err
	}

	return nil
}

func recvFrame(c net.Conn, deadline time.Time) ([]byte, error) {
	if err := c.SetReadDeadline(deadline); err != nil {
		return nil, err
	}

	hdr := make([]byte, 4)
	if _, err := io.ReadFull(c, hdr); err != nil {
		return nil, err
	}
	size := binary.BigEndian.Uint32(hdr)
	if size > maxFrameSize {
		return nil, errFrameTooLarge
	}
	frame := make([]byte, size)
	if _, err := io.ReadFull(c, frame); err != nil {
		return nil, err
	}

	if err := removeDeadline(c); err != nil {
		return nil, err
	}

	return frame, nil
}

func removeDeadline(c net.Conn) error { return c.SetDeadline(time.Time{}) }
