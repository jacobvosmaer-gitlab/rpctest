package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"

	"gitlab.com/jacobvosmaer-gitlab/rpctest/transport"
)

var rpcAddr string

func main() {
	if len(os.Args) != 3 {
		fmt.Println("Usage: rpcproxy LISTEN_ADDR RPC_ADDR")
		os.Exit(1)
	}

	rpcAddr = os.Args[2]
	if err := _main(os.Args[1]); err != nil {
		log.Fatal(err)
	}
}

func _main(listenAddr string) error {
	l, err := net.Listen("tcp", listenAddr)
	if err != nil {
		return err
	}
	defer l.Close()

	return transport.NewServer(handle).Serve(l)
}

func handle(sClient *transport.ServerSession, req []byte) error {
	log.Printf("rpcproxy: %s", req)

	cServer, err := transport.Call(transport.DialNet("tcp", rpcAddr), req)
	if err != nil {
		return err
	}
	defer cServer.Close()

	cClient, err := sClient.Accept()
	if err != nil {
		return err
	}

	errC := make(chan error, 2)
	go func() {
		_, err := io.Copy(cServer, cClient)
		errC <- err
	}()
	go func() {
		_, err := io.Copy(cClient, cServer)
		errC <- err
	}()

	// Intentionally wait for only one copy goroutine to finish. Once one
	// finishes, we must unblock the other by calling close, and that will
	// always return an error so there is no point in checking and logging
	// that error.
	return <-errC
}
